.. image:: https://s.weblate.org/cdn/Logo-Darktext-borders.png
   :alt: Weblate
   :target: https://weblate.org/
   :height: 80px

**Weblate is libre software web-based continuous localization system,
used by over 2500 libre projects and companies in more than 165 countries.**

The docker-compose for Docker container for Weblate

.. image:: https://readthedocs.org/projects/weblate/badge/
    :alt: Documentation
    :target: https://docs.weblate.org/en/latest/admin/install/docker.html

Documentation
-------------

Detailed documentation is available in Weblate documentation:

https://docs.weblate.org/en/latest/admin/install/docker.html

The Translate.Codeberg.org Configuration
----------------------------------------

1. Content of  ``docker-compose.override.yml``

   .. code-block:: yml

        version: '3'
        services:
          weblate:
            image: codeberg.org/codeberg-infrastructure/weblate
            ports:
              - 80:8080
            logging:
              driver: journald
            environment:
              WEBLATE_DEBUG: 0
              WEBLATE_WORKERS: 4
              WEBLATE_SITE_DOMAIN: translate.codeberg.org
              WEBLATE_SITE_TITLE: Codeberg Translate
              WEBLATE_EMAIL_HOST: smtp.codeberg.org
              WEBLATE_EMAIL_HOST_USER: translate
              WEBLATE_EMAIL_HOST_PASSWORD: <SMTP_PASS>
              WEBLATE_SERVER_EMAIL: translate@codeberg.org
              WEBLATE_DEFAULT_FROM_EMAIL: weblate@codeberg.org
              WEBLATE_ADMIN_EMAIL: mhoffmann@codeberg.org
              WEBLATE_ENABLE_HTTPS: 1
              WEBLATE_GET_HELP_URL: https://docs.codeberg.org/
              WEBLATE_PRIVACY_URL: https://codeberg.org/codeberg/org/src/PrivacyPolicy.md
              WEBLATE_LEGAL_URL: https://codeberg.org/Codeberg/org/src/branch/main/Imprint.md
              WEBLATE_GPG_IDENTITY: Codeberg Translate <translate@codeberg.org>
              WEBLATE_DEFAULT_COMMITER_EMAIL: translate@codeberg.org
              WEBLATE_DEFAULT_COMMITER_NAME: Codeberg Translate
              WEBLATE_ENABLE_SHARING: 1
              WEBLATE_AVATAR_URL_PREFIX: https://codeberg.org/user/
              WEBLATE_GITEA_HOST: codeberg.org
              WEBLATE_GITEA_USERNAME: translate
              WEBLATE_GITEA_TOKEN: <Token>
              WEBLATE_SOCIAL_AUTH_GITEA_KEY: <Key>
              WEBLATE_SOCIAL_AUTH_GITEA_API_URL: https://codeberg.org
              WEBLATE_SOCIAL_AUTH_GITEA_SECRET: <Secret>
              WEBLATE_NO_EMAIL_AUTH: 1
              WEBLATE_PRIVATE_COMMIT_EMAIL_OPT_IN: 0
              WEBLATE_PRIVATE_COMMIT_EMAIL_TEMPLATE: '{username}@noreply.codeberg.org'
              CLIENT_MAX_BODY_SIZE: 500M
          cache:
            logging:
              driver: journald
          database:
            logging:
              driver: journald




2. Start it up:

   .. code-block:: shell

        docker-compose pull  # only necessary on upgrade
        docker-compose up -d


3. Reverse proxy needs to pass at least ``X-Forwarded-For`` and ``X-Forwarded-Proto``.

4. All persistent data is stored in ``/var/lib/docker/volumes/weblate-docker_*``
   We need to modify some config directly in there:
   Some custom config in ``/var/lib/docker/volumes/weblate-docker_weblate-data/_data/settings-override.py``. 
   This set's the Codeberg icon/name for the oauth provider (normally this is only briefly visiable
   before redirection, but better to have it than not.)

   .. code-block:: python

        SOCIAL_AUTH_GITEA_TITLE = "Codeberg"
        SOCIAL_AUTH_GITEA_IMAGE = "codeberg.png"

5. ``codeberg.png`` needs to go into ``/var/lib/docker/volumes/weblate-docker_weblate-data/_data/python/customize/static/auth``

6. To make the avatar fetching from codeberg.org work correctly we need to patch the logic in weblate.
   This is done via `this <https://codeberg.org/Codeberg-Infrastructure/weblate-docker/src/branch/main/patches/0001-download-avatars-from-codeberg.org.patch>` patch and rebuilding the docker container.

7. Admin (non-oauth) login at https://translate.codeberg.org/admin/login, then "return to weblate" in top-right to get out of the django admin backend.

8. Every user is allowed to create new translation projects. This is enabled by following https://github.com/WeblateOrg/weblate/issues/4842#issuecomment-737383211:
   
   * In the django admin interface: Create new Role (named something like "Create Projects") with the "Add new projects (site-wide permission)"
   * in https://translate.codeberg.org/manage/groups/ -> ``Users`` -> Edit -> In addition to the "power users" role, add the newly created "Create projects" role.


Rebuilding the weblate docker image
-----------------------------------

The weblate docker image is built from https://codeberg.org/Codeberg-Infrastructure/weblate-docker.
